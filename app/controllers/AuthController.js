const Models = require('../models/index');

module.exports = {
    hello: function (req, res) {
        res.render('index', { title: 'Quick MVC' });
    },
    login: function (req, res) {
        res.render('login', { title: 'Login Page' });
    },
    dashboard: function (req, res) {
        res.render('dashboard',{title: 'Dashboard - BillMates', heading: 'Dashboard', isActive: 'dashboard', user: req.user});
    },
    editProfile: function (req, res) {

        const data = req.body;

        Models.users.findOne({where: {id: req.user.id}}).then(function(result){
           
            
            result.update({
                name : req.body.name,
                email : req.body.email
           
           }).then(function(result){
               console.log(req.body);
               req.user.name = req.body.name;
               console.log("user updated");
           }).catch(function(errors){
               console.log(errors);
           });
        }).catch(function(errors){
            console.log(errors);
        });

         res.redirect('/profile');
    },
    signup: function (req, res) {
        res.render('signup', { title: 'Signup' });
    },
    profile: function (req, res) {
        res.render('profile',{title: 'User Profile - BillMates', heading: 'User Profile', isActive: 'profile', user: req.user});
    },
    summary: function (req, res) {

        //get start and this of month 
        var date = new Date(), y = date.getFullYear(), m = date.getMonth();
        var firstDay = new Date(y, m, 1);
        var lastDay = new Date(y, m + 1, 0);
        ///
        
        Models.expenses.sum('amount',{
            where: { user_id: req.user.id,
                
                date: {
                    $gt: firstDay,
                    $lt: lastDay
                }
            
            },

        }).then(function(monthSum) {

        
        //get start and this year
        var date = new Date(), y = date.getFullYear();
        var yearFirstDay = new Date(y, 0, 1);
        var yearLastDay = new Date(y, 11, 31);
        ///
            
            Models.expenses.sum('amount',{
                where: { user_id: req.user.id,
                    
                    date: {
                        $gt: yearFirstDay,
                        $lt: yearLastDay
                    }
                
                },
    
            }).then(function(yearSum) {

                        //get lifetime expense
                
                
                    
                    Models.expenses.sum('amount',{
                        where: { user_id: req.user.id,

                        },
            
                    }).then(function(lifetimeSum) {
                        console.log(yearSum);
                        res.render('summary',{title: 'Expense Summary - BillMates', heading: 'Expense Summary', isActive: 'summary', user: req.user,lifetimeSum:lifetimeSum, yearSum:yearSum, monthSum:monthSum});
                    });


            });

            
        });

        
    },
    expenses: function (req, res) {

        Models.expenses.findAll({
            where: { user_id: req.user.id }

        }).then(function(expenses) {
            res.render('expenses',{title: 'Expenses - BillMates', heading: 'Expenses', isActive: 'expenses', user: req.user, expenses:expenses});
        });


        
    },
    addExpenseRender: function (req, res) {
        res.render('addExpense',{title: 'Add New Expense - BillMates', heading: 'Add New Expense', isActive: 'expenses', user: req.user});
    },
    addExpense: function(req, res) {

        const exp = req.body;
        console.log(exp);
       
            Models.expenses.create({
                user_id: req.user.id,
                name: exp.expense,
                amount: exp.amount,
                description: exp.description,
                date: exp.date

            }).then(function(result) {
                console.log("Expense Added");
                
                 res.redirect('/expenses');;
            })
            
            
        
    },
    editExpense: function (req, res) {
        
        const id = req.params.id;
        const exp = req.body;

        
        Models.expenses.update({
             name: exp.expense,
             amount: exp.amount,
             description: exp.description
             


        }, { where: { id: id } }).then(function() {
            console.log("Expense Updated");

             res.redirect('/expenses');


        })
        
        
  },
  editExpenseRender: function (req, res) {
        
    const id = req.params.id;

    Models.expenses.findByPk(id).then(function(expense) {
       console.log("Expense Updated");

       res.render('editExpense',{title: 'Edit Expense - BillMates', heading: 'Edit Expense', isActive: 'expenses', user: req.user, expense:expense});


   })


  },
   
}