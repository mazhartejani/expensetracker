const AuthController = require('./controllers/AuthController');
const SiteConroller = require('./controllers/SiteController');
module.exports = function (app, passport) {
	app.get('/login', AuthController.login);
	app.get('/dashboard', _authenticationMiddleware , AuthController.dashboard);
	app.get('/profile', _authenticationMiddleware , AuthController.profile);
	app.get('/summary', _authenticationMiddleware , AuthController.summary);
	app.get('/expenses', _authenticationMiddleware , AuthController.expenses);
	app.get('/expenses/addExpense', _authenticationMiddleware , AuthController.addExpenseRender);
	app.post('/expenses/addExpense', _authenticationMiddleware , AuthController.addExpense);

	app.get('/expenses/editExpense/:id', _authenticationMiddleware , AuthController.editExpenseRender);
	app.post('/expenses/editExpense/:id', _authenticationMiddleware , AuthController.editExpense);

	app.post('/profile', _authenticationMiddleware , AuthController.editProfile);

	app.post('/login', passport.authenticate('local', {
		successRedirect : '/expenses', // redirect to the secure dashboard section
		failureRedirect : '/login', // redirect back to the signup page if there is an error
		failureFlash : true // allow flash messages
	}));
	app.get('/signup', AuthController.signup);
	app.post('/signup',passport.authenticate('local-signup', {
        successRedirect : '/expenses', // redirect to the secure dashboard section
        failureRedirect : '/signup', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));//implement passport signup strategy
	 app.get('/', SiteConroller.index);
	// app.get('/contact', SiteConroller.contact);
	// app.post('/contact', SiteConroller.contact);
	// app.get('/faq', SiteConroller.faq);
	// Endpoint to logout
	app.get('/logout', function (req, res) {
		req.logout();
		//res.send(null);
		return res.redirect('/login');
	});
}

function _authenticationMiddleware(req, res, next) {
	
			if (req.isAuthenticated()) {
				return next()
			}
			res.redirect('/login')

}